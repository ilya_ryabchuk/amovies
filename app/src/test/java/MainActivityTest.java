import android.app.Activity;

import com.ilyarb.amovies.BuildConfig;
import com.ilyarb.amovies.ui.activity.MainActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 17, manifest = "src/main/AndroidManifest.xml")
public class MainActivityTest {

	@Test
	public void testSetupActionBar() throws Exception {
		Activity activity = Robolectric.setupActivity(MainActivity.class);
		assertTrue(activity.getActionBar() != null);
	}

}