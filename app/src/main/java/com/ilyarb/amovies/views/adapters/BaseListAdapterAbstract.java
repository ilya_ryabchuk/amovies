package com.ilyarb.amovies.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ilyarb.amovies.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class BaseListAdapterAbstract
	extends RecyclerView.Adapter<BaseListAdapterAbstract.ViewHolder> {

	public interface ClickListener {
		void itemClicked(View v, int position);
	}

	ClickListener clickListener;

	public BaseListAdapterAbstract() { }

	public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		@Bind(R.id.data_title)
		TextView itemTitle;

		@Bind(R.id.data_overview)
		TextView itemOverview;

		@Bind(R.id.data_poster)
		ImageView itemPoster;

		@Bind(R.id.data_rate)
		RatingBar itemRate;

		public ViewHolder(View v) {
			super(v);

			ButterKnife.bind(this, v);
			v.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			clickListener.itemClicked(v, getLayoutPosition());
		}
	}

	public void setClickListener(ClickListener l) {
		this.clickListener = l;
	}

}
