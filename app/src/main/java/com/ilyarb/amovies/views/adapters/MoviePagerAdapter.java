package com.ilyarb.amovies.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.ilyarb.amovies.ui.fragments.MovieFragment;

public class MoviePagerAdapter extends BasePagerAdapterAbstract {

	private static final CharSequence[] TAB_TITLES = new CharSequence[] {
		"IN THEATERS",
		"UPCOMING",
		"POPULAR"
	};

	private static final String[] REQUEST_TYPES = new String[] {
		"now_playing",
		"popular",
		"top_rated"
	};

	@Override
	public Fragment getItem(int position) {
		return MovieFragment.newInstance(REQUEST_TYPES[position]);
	}

	public MoviePagerAdapter(FragmentManager fm) {
		super(fm, TAB_TITLES);
	}

}
