package com.ilyarb.amovies.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ilyarb.amovies.Config;
import com.ilyarb.amovies.R;
import com.ilyarb.amovies.models.Movie;
import com.ilyarb.amovies.utils.TextUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieListAdapter extends BaseListAdapterAbstract {

	private List<Movie> movies;

	private Context context;

	public MovieListAdapter(final List<Movie> movies, final Context context) {
		this.movies = movies;
		this.context = context;
	}

	@Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext())
			.inflate(R.layout.data_card, parent, false);

		return new ViewHolder(v);
	}

	@Override public void onBindViewHolder(ViewHolder holder, final int position) {

		holder.itemTitle.setText(movies.get(position).getTitle());
		// Truncate overview that more cards can fit on screen
		String overview = TextUtils.truncate(movies.get(position).getOverview());
		holder.itemOverview.setText(overview);

		// Set movie poster (loading poster from image server)
		Picasso.with(context)
			// Making the full url to image
			.load(Config.IMAGE_URL + movies.get(position).getPosterPath())
			.fit()
			.centerCrop()
			.into(holder.itemPoster);

		// Set movie rating
		holder.itemRate.setNumStars(10);
		holder.itemRate.setMax(10);
		holder.itemRate.setRating(movies.get(position).getVoteAverage());
	}

	@Override public int getItemCount() {
		return movies.size();
	}

}
