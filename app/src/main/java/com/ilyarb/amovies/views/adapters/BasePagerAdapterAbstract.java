package com.ilyarb.amovies.views.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public abstract class BasePagerAdapterAbstract extends FragmentStatePagerAdapter {

	private final CharSequence[] tabTitles;

	public static final int TABS_COUNT = 3;

	public BasePagerAdapterAbstract(FragmentManager fm,
	                                CharSequence[] tabTitles) {
		super(fm);
		this.tabTitles = tabTitles;
	}

	@Override
	public int getCount() {
		return TABS_COUNT;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabTitles[position];
	}

}
