package com.ilyarb.amovies.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.ilyarb.amovies.ui.fragments.TVFragment;

public class TVPagerAdapter extends BasePagerAdapterAbstract {

	private static final CharSequence[] TAB_TITLES = new CharSequence[] {
		"TODAY",
		"TOP RATED",
		"POPULAR"
	};

	private static final String[] REQUEST_TYPES = new String[] {
		"airing_today",
		"top_rated",
		"popular"
	};

	@Override
	public Fragment getItem(int position) {
		return TVFragment.newInstance(REQUEST_TYPES[position]);
	}

	public TVPagerAdapter(FragmentManager fm) {
		super(fm, TAB_TITLES);
	}

}
