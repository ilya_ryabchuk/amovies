package com.ilyarb.amovies.utils;

public class TextUtils {

	/**
	 *
	 * @param text Initial text to truncate
	 * @return truncated text to TEXT_SIZE
	 */

	public static String truncate(String text) {
		final int TEXT_SIZE = 200;

		if (text == null || text.length() < TEXT_SIZE) {
			return text;
		}

		text = text.substring(0, TEXT_SIZE) + "...";
		return text;
	}

}
