package com.ilyarb.amovies.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetUtils {

	/**
	 * Checks if there a internet connection
	 * @param context to get system service
	 * @return connection status
	 */

	public static boolean isConnectedInternet(Context context) {
		return isConnectedWifi(context) || isConnectedMobile(context);
	}

	/**
	 * Check if there any connection to Wifi network
	 * @param context to get system service
	 * @return connection status
	 */

	private static boolean isConnectedWifi(Context context) {
		try {
			String cs = Context.CONNECTIVITY_SERVICE;
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(cs);
			NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			int TRY_COUNT = 10;
			boolean status = false;

			for (int i = 0; i < TRY_COUNT; i++) {
				if (wifi != null && wifi.isConnected()) {
					status = true;
					break;
				}
			}
			return status;

		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Check if there any connection to Mobile Network
	 * @param context to get system service
	 * @return connection status
	 */

	private static boolean isConnectedMobile(Context context) {
		try {
			String cs = Context.CONNECTIVITY_SERVICE;
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(cs);
			NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			int TRY_COUNT = 10;
			boolean status = false;

			for (int i = 0; i < TRY_COUNT; i++) {
				if (mobile != null && mobile.isConnected()) {
					status = true;
					break;
				}
			}
			return status;

		} catch (Exception e) {
			return false;
		}
	}

}
