package com.ilyarb.amovies.models;

import io.realm.RealmList;
import io.realm.RealmObject;

public class MovieResultsPage extends RealmObject {

	private String type;

	private int page;

	private int totalPages;

	private int totalResults;

	private RealmList<Movie> results;

	public MovieResultsPage() {}

	/* Getters
	================================== */

	public String getType() {
		return type;
	}

	public int getPage() {
		return page;
	}

	public RealmList<Movie> getResults() {
		return results;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public int getTotalResults() {
		return totalResults;
	}

	/* Setters
	================================== */

	public void setType(String type) {
		this.type = type;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setResults(RealmList<Movie> results) {
		this.results = results;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}
}
