package com.ilyarb.amovies.models;

import io.realm.RealmObject;

public class Backdrop extends RealmObject {

	private String filePath;

	private int width;

	private int height;

	public Backdrop() {}

	/* Getters
	================================== */

	public String getFilePath() {
		return filePath;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	/* Setters
	================================== */

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
