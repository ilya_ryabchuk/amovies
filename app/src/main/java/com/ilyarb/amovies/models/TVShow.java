package com.ilyarb.amovies.models;

import io.realm.RealmObject;

public class TVShow extends RealmObject {

	private int id;

	private String name;

	private String posterPath;

	private String overview;

	private String status;

	private String numberOfEpisodes;

	private String numberOfSeasons;

	private String homepage;

	private float voteAverage;

	private String firstAirDate;

	public TVShow() {}

	/* Getters
	================================== */

	public String getFirstAirDate() {
		return firstAirDate;
	}

	public String getHomepage() {
		return homepage;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNumberOfEpisodes() {
		return numberOfEpisodes;
	}

	public String getNumberOfSeasons() {
		return numberOfSeasons;
	}

	public String getOverview() {
		return overview;
	}

	public String getPosterPath() {
		return posterPath;
	}

	public String getStatus() {
		return status;
	}

	public float getVoteAverage() {
		return voteAverage;
	}

	/* Setters
	================================== */

	public void setFirstAirDate(String firstAirDate) {
		this.firstAirDate = firstAirDate;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumberOfEpisodes(String numberOfEpisodes) {
		this.numberOfEpisodes = numberOfEpisodes;
	}

	public void setNumberOfSeasons(String numberOfSeasons) {
		this.numberOfSeasons = numberOfSeasons;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setPosterPath(String posterPath) {
		this.posterPath = posterPath;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setVoteAverage(float voteAverage) {
		this.voteAverage = voteAverage;
	}

}
