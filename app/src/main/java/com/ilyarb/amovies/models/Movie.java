package com.ilyarb.amovies.models;

import io.realm.RealmObject;

public class Movie extends RealmObject {

	private int id;

	private String title;

	private String overview;

	private String posterPath;

	private String releaseDate;

	private String homepage;

	private String budget;

	private float voteAverage;

	private int voteCount;

	private String status;

	private String tagline;

	private TrailerResultsPage trailers;

	private Images images;

	public Movie() {}

	/* Getters
	================================== */

	public String getBudget() {
		return budget;
	}

	public String getHomepage() {
		return homepage;
	}

	public int getId() {
		return id;
	}

	public String getOverview() {
		return overview;
	}

	public String getPosterPath() {
		return posterPath;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public String getTitle() {
		return title;
	}

	public TrailerResultsPage getTrailers() {
		return trailers;
	}

	public float getVoteAverage() {
		return voteAverage;
	}

	public Images getImages() {
		return images;
	}

	public int getVoteCount() {
		return voteCount;
	}

	public String getStatus() {
		return status;
	}

	public String getTagline() {
		return tagline;
	}

	/* Setters
	================================== */

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setPosterPath(String posterPath) {
		this.posterPath = posterPath;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTrailers(TrailerResultsPage trailers) {
		this.trailers = trailers;
	}

	public void setVoteAverage(float voteAverage) {
		this.voteAverage = voteAverage;
	}

	public void setImages(Images images) {
		this.images = images;
	}

	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}
}
