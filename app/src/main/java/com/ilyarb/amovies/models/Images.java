package com.ilyarb.amovies.models;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Images extends RealmObject {

	private int id;

	private RealmList<Backdrop> backdrops;

	public Images() {}

	/* Getters
	================================== */

	public RealmList<Backdrop> getBackdrops() {
		return backdrops;
	}

	public int getId() {
		return id;
	}

	/* Setters
	================================== */

	public void setBackdrops(RealmList<Backdrop> backdrops) {
		this.backdrops = backdrops;
	}

	public void setId(int id) {
		this.id = id;
	}
}
