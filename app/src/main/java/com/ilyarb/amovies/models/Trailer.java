package com.ilyarb.amovies.models;

import io.realm.RealmObject;

public class Trailer extends RealmObject {

	private String id;

	private String key;

	private String name;

	private String site;

	public Trailer() {}

	/* Getters
	================================== */

	public String getId() {
		return id;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

	public String getSite() {
		return site;
	}

	/* Setters
	================================== */

	public void setId(String id) {
		this.id = id;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSite(String site) {
		this.site = site;
	}
}
