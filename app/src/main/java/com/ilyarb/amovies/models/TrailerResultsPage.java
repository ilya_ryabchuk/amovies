package com.ilyarb.amovies.models;

import io.realm.RealmList;
import io.realm.RealmObject;

public class TrailerResultsPage extends RealmObject {

	private int id;

	private RealmList<Trailer> results;

	public TrailerResultsPage() {}

	/* Getters
	================================== */

	public int getId() {
		return id;
	}

	public RealmList<Trailer> getResults() {
		return results;
	}

	/* Setters
	================================== */

	public void setId(int id) {
		this.id = id;
	}

	public void setResults(RealmList<Trailer> results) {
		this.results = results;
	}
}
