package com.ilyarb.amovies.db;

import android.support.annotation.NonNull;

import com.ilyarb.amovies.models.TVResultsPage;
import com.ilyarb.amovies.models.TVShow;

import io.realm.Realm;
import io.realm.RealmQuery;

public class TVHelper {

	public static void savePage(@NonNull Realm realm, final TVResultsPage page) {

		realm.beginTransaction();
		realm.clear(TVResultsPage.class);
		realm.copyToRealm(page);
		realm.commitTransaction();
		realm.close();
	}

	public static void save(@NonNull Realm realm, TVShow show) {

		realm.beginTransaction();
		realm.clear(TVShow.class);
		realm.copyToRealm(show);
		realm.commitTransaction();

	}

	@NonNull
	public static RealmQuery<TVShow> get(@NonNull Realm realm, final int id) {
		return realm.where(TVShow.class)
			.equalTo("id", id);
	}

	@NonNull
	public static TVResultsPage getPage(@NonNull Realm realm, String type) {
		return realm.where(TVResultsPage.class)
			.equalTo("type", type)
			.findFirst();
	}

}
