package com.ilyarb.amovies.db;

import android.support.annotation.NonNull;

import com.ilyarb.amovies.models.Movie;
import com.ilyarb.amovies.models.MovieResultsPage;

import io.realm.Realm;

public class MovieHelper {

	public static void savePage(@NonNull Realm realm, final MovieResultsPage page) {

		realm.beginTransaction();
		realm.clear(MovieResultsPage.class);
		realm.copyToRealm(page);
		realm.commitTransaction();
		realm.close();
	}

	public static void save(@NonNull Realm realm, Movie movie) {

		realm.beginTransaction();
		realm.clear(Movie.class);
		realm.copyToRealm(movie);
		realm.commitTransaction();

	}

	@NonNull
	public static Movie get(@NonNull Realm realm, final int id) {
		return realm.where(Movie.class)
			.equalTo("id", id)
			.findFirst();
	}

	@NonNull
	public static MovieResultsPage gePage(@NonNull Realm realm, String key) {
		return realm.where(MovieResultsPage.class)
			.equalTo("type", key)
			.findFirst();
	}

}
