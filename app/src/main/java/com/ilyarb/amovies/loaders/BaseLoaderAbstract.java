package com.ilyarb.amovies.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.ilyarb.amovies.network.response.RequestResult;
import com.ilyarb.amovies.network.response.Response;

import java.io.IOException;

public abstract class BaseLoaderAbstract extends AsyncTaskLoader<Response> {

	private static final String TAG = "BASE LOADER";

	public BaseLoaderAbstract(Context context) {
		super(context);
	}

	@Override protected void onStartLoading() {
		super.onStartLoading();
		forceLoad();
	}

	@Override public Response loadInBackground() {
		try {
			Response response = apiCall();
			if (response.getRequestResult() == RequestResult.SUCCESS) {
				response.save(getContext());
			}

			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return new Response();
		}
	}

	protected abstract Response apiCall() throws IOException;
}
