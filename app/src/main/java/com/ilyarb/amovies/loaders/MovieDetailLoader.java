package com.ilyarb.amovies.loaders;

import android.content.Context;

import com.ilyarb.amovies.models.Movie;
import com.ilyarb.amovies.models.TrailerResultsPage;
import com.ilyarb.amovies.network.ApiFactory;
import com.ilyarb.amovies.network.IMovieService;
import com.ilyarb.amovies.network.response.MovieDetailResponse;
import com.ilyarb.amovies.network.response.RequestResult;
import com.ilyarb.amovies.network.response.Response;

import java.io.IOException;

public class MovieDetailLoader extends BaseLoaderAbstract {

	private int id;

	public MovieDetailLoader(final Context context, final int id) {
		super(context);
		this.id = id;
	}

	@Override protected Response apiCall() throws IOException {

		IMovieService service = ApiFactory.getMovieService();

		Movie response = service.movie(id);
		TrailerResultsPage trailers = service.movieTrailers(id);

		response.setTrailers(trailers);

		return new MovieDetailResponse()
			.setRequestResult(RequestResult.SUCCESS)
			.setAnswer(response);
	}
}
