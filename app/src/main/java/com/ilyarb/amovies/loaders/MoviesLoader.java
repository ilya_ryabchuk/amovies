package com.ilyarb.amovies.loaders;

import android.content.Context;

import com.ilyarb.amovies.models.MovieResultsPage;
import com.ilyarb.amovies.network.ApiFactory;
import com.ilyarb.amovies.network.IMovieService;
import com.ilyarb.amovies.network.response.MovieResponse;
import com.ilyarb.amovies.network.response.RequestResult;
import com.ilyarb.amovies.network.response.Response;

import java.io.IOException;

public class MoviesLoader extends BaseLoaderAbstract {

	private final String type;

	public MoviesLoader(final Context context, final String type) {
		super(context);
		this.type = type;
	}

	@Override protected Response apiCall() throws IOException {

		IMovieService service = ApiFactory.getMovieService();
		MovieResultsPage page = service.movies(type);
		page.setType(type);

		return new MovieResponse()
			.setRequestResult(RequestResult.SUCCESS)
			.setAnswer(page);
	}
}
