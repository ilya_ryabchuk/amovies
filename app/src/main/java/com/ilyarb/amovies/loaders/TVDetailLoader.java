package com.ilyarb.amovies.loaders;

import android.content.Context;

import com.ilyarb.amovies.models.TVShow;
import com.ilyarb.amovies.network.ApiFactory;
import com.ilyarb.amovies.network.ITVShowsService;
import com.ilyarb.amovies.network.response.RequestResult;
import com.ilyarb.amovies.network.response.Response;
import com.ilyarb.amovies.network.response.TVDetailResponse;

import java.io.IOException;

public class TVDetailLoader extends BaseLoaderAbstract {

	private int id;

	public TVDetailLoader(final Context context, final int id) {
		super(context);
		this.id = id;
	}

	@Override protected Response apiCall() throws IOException {
		ITVShowsService service = ApiFactory.getTVService();
		TVShow show = service.show(id);

		return new TVDetailResponse()
			.setRequestResult(RequestResult.SUCCESS)
			.setAnswer(show);
	}
}
