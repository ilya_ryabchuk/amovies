package com.ilyarb.amovies.loaders;

import android.content.Context;

import com.ilyarb.amovies.models.TVResultsPage;
import com.ilyarb.amovies.network.ApiFactory;
import com.ilyarb.amovies.network.ITVShowsService;
import com.ilyarb.amovies.network.response.RequestResult;
import com.ilyarb.amovies.network.response.Response;
import com.ilyarb.amovies.network.response.TVResponse;

import java.io.IOException;

public class TVShowsLoader extends BaseLoaderAbstract {

	private final String type;

	public TVShowsLoader(final Context context, final String type) {
		super(context);
		this.type = type;
	}

	@Override protected Response apiCall() throws IOException {

		ITVShowsService service = ApiFactory.getTVService();
		TVResultsPage page = service.shows(type);
		page.setType(type);

		return new TVResponse()
			.setRequestResult(RequestResult.SUCCESS)
			.setAnswer(page);
	}

}
