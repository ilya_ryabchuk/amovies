package com.ilyarb.amovies.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ilyarb.amovies.R;
import com.ilyarb.amovies.network.response.Response;
import com.ilyarb.amovies.utils.NetUtils;

import butterknife.Bind;

public abstract class BaseDetailsActivityAbstract extends AppCompatActivity
	implements LoaderManager.LoaderCallbacks<Response> {

	public interface UIHandler {
		void handle(Message msg);
	}

	protected static final String KEY = "item_id";

	@Bind(R.id.toolbar)
	protected Toolbar toolbar;

	@Bind(R.id.app_bar)
	protected AppBarLayout appBar;

	@Bind(R.id.item_content)
	protected LinearLayout itemContent;

	@Bind(R.id.progress_bar)
	protected ProgressBar progressBar;

	@Bind(R.id.error_block)
	LinearLayout errorBlock;

	@Bind(R.id.toolbar_layout)
	protected CollapsingToolbarLayout collapsingLayout;

	protected ActivityHandler activityHandler;

	protected void setupActionBar() {
		setSupportActionBar(toolbar);
	}

	protected void showHeader() {
		progressBar.setVisibility(View.GONE);
		appBar.setVisibility(View.VISIBLE);
		itemContent.setVisibility(View.VISIBLE);
	}

	protected void showError() {
		errorBlock.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.GONE);
	}

	protected void loadData(Bundle args, int id) {
		if (NetUtils.isConnectedInternet(this)) {
			getSupportLoaderManager().initLoader(id, args, this);
		} else {
			loadFromCache();
		}
	}

	protected void reloadData(Bundle args, int id) {
		if (NetUtils.isConnectedInternet(this)) {
			getSupportLoaderManager().restartLoader(id, args, this);
		} else {
			loadFromCache();
		}
	}

	protected static class ActivityHandler extends Handler {

		private final UIHandler handler;

		public ActivityHandler(final UIHandler handler) {
			this.handler = handler;
		}

		@Override
		public void handleMessage(Message msg) {
			handler.handle(msg);
		}
	}

	protected void loadFromCache() {}

	protected void setDefaultValues() {}

}