package com.ilyarb.amovies.ui.notifications;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

public class MovieNotification {

	private String title;

	private int smallIcon;

	private String text;

	private NotificationCompat.Builder mBuilder;

	public MovieNotification(Context context) {
		mBuilder = new NotificationCompat.Builder(context);
	}

	public Notification makeNotification() {
		return mBuilder
			.setSmallIcon(smallIcon)
			.setContentTitle(title)
			.setContentText(text)
			.build();
	}

	public void setSmallIcon(int smallIcon) {
		this.smallIcon = smallIcon;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getSmallIcon() {
		return smallIcon;
	}

	public String getText() {
		return text;
	}

	public String getTitle() {
		return title;
	}
}
