package com.ilyarb.amovies.ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ilyarb.amovies.R;
import com.ilyarb.amovies.network.response.Response;
import com.ilyarb.amovies.utils.NetUtils;
import com.ilyarb.amovies.views.adapters.BaseListAdapterAbstract;

import java.lang.ref.WeakReference;

import butterknife.Bind;

public abstract class BaseFragmentAbstract extends Fragment
	implements
	LoaderManager.LoaderCallbacks<Response>,
	BaseListAdapterAbstract.ClickListener {

	protected static final String KEY = "request_type";

	@Bind(R.id.results_recycler_view)
	protected RecyclerView recyclerView;

	@Bind(R.id.progress_bar)
	protected ProgressBar progressBar;

	@Bind(R.id.error_block)
	protected LinearLayout errorBlock;

	protected static class UIHandler extends Handler {

		private final WeakReference<FragmentActivity> mActivity;

		public UIHandler(FragmentActivity activity) {
			this.mActivity = new WeakReference<>(activity);
		}

		@Override public void handleMessage(Message msg) {
			FragmentActivity activity = mActivity.get();
			if (activity != null) {
				// TODO
			}
		}
	}

	protected void showError() {
		errorBlock.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.GONE);
	}

	protected void showData() {
		recyclerView.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.GONE);
	}

	protected void loadData(int id, Bundle args) {
		if (NetUtils.isConnectedInternet(getActivity())) {
			getLoaderManager().initLoader(id, args, this);
		} else {
			loadFromCache();
		}
	}

	protected void setupRecyclerView() {}

	protected void loadFromCache() {}

}
