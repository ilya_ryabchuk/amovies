package com.ilyarb.amovies.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ilyarb.amovies.R;
import com.ilyarb.amovies.db.TVHelper;
import com.ilyarb.amovies.loaders.TVShowsLoader;
import com.ilyarb.amovies.models.TVResultsPage;
import com.ilyarb.amovies.models.TVShow;
import com.ilyarb.amovies.network.response.Response;
import com.ilyarb.amovies.ui.activity.TVDetailsActivity;
import com.ilyarb.amovies.views.adapters.TVListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class TVFragment extends BaseFragmentAbstract {

	private final List<TVShow> shows = new ArrayList<>();

	private TVListAdapter adapter;

	public static TVFragment newInstance(final String type) {
		TVFragment f = new TVFragment();
		Bundle args = new Bundle();

		args.putString(KEY, type);
		f.setArguments(args);

		return f;
	}

	@Nullable
	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.result_fragment, container, false);
		ButterKnife.bind(this, v);

		adapter = new TVListAdapter(shows, getActivity());
		adapter.setClickListener(this);

		setupRecyclerView();

		loadData(R.id.tv_shows_loader, getArguments());

		return v;
	}

	@Override public Loader<Response> onCreateLoader(int id, Bundle args) {
		return new TVShowsLoader(getActivity(), args.getString(KEY));
	}

	@Override public void onLoadFinished(Loader<Response> loader, Response data) {
		TVResultsPage response = data.getTypedAnswer();

		if (response != null) {
			shows.addAll(response.getResults());
			adapter.notifyDataSetChanged();
			showData();

		} else {
			showError();
		}

		getLoaderManager().destroyLoader(loader.getId());
	}

	@Override public void onLoaderReset(Loader<Response> loader) {}

	@Override public void itemClicked(View view, int position) {
		int movieId = shows.get(position).getId();
		Intent i = new Intent(getActivity(), TVDetailsActivity.class);
		i.putExtra("item_id", movieId);
		startActivity(i);
	}

	@Override protected void setupRecyclerView() {
		RecyclerView.LayoutManager lm = new LinearLayoutManager(getActivity());
		recyclerView.setHasFixedSize(true);
		recyclerView.setItemAnimator(new DefaultItemAnimator());
		recyclerView.setLayoutManager(lm);
		recyclerView.setAdapter(adapter);
	}

	@Override protected void loadFromCache() {
		TVResultsPage page =
			TVHelper.getPage(Realm.getInstance(getActivity()), getArguments().getString(KEY));

		if (page == null) {
			showError();

		} else {
			if (page.getResults().size() != 0) {
				shows.addAll(page.getResults());
				adapter.notifyDataSetChanged();
				showData();
			} else {
				showError();
			}
		}
	}

	@OnClick(R.id.button_retry)
	protected void retry(View v) {
		getLoaderManager().restartLoader(R.id.tv_shows_loader, getArguments(), this);
		errorBlock.setVisibility(View.GONE);
		progressBar.setVisibility(View.VISIBLE);
	}
}
