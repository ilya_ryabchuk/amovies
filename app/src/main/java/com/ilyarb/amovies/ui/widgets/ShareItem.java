package com.ilyarb.amovies.ui.widgets;

public class ShareItem {

	private String text;

	private String homepage;

	public ShareItem() {}

	public String getText() {
		return text;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public void setText(String text) {
		this.text = text;
	}
}
