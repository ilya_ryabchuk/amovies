package com.ilyarb.amovies.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ilyarb.amovies.App;
import com.ilyarb.amovies.R;
import com.ilyarb.amovies.views.adapters.MoviePagerAdapter;
import com.ilyarb.amovies.views.adapters.TVPagerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

	@Bind(R.id.toolbar)
	Toolbar mToolbar;

	@Bind(R.id.navigation_view)
	NavigationView mNavigationView;

	@Bind(R.id.drawer_layout)
	DrawerLayout mDrawerLayout;

	@Bind(R.id.view_pager)
	ViewPager mViewPager;

	@Bind(R.id.tab_layout)
	TabLayout mTabLayout;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Check for settings if notifications enabled
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		if (prefs.getBoolean("notifications", true)) {
			App app = (App) getApplication();
			app.enableNotifications();
		}

		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		setupNavigation();

		mViewPager.setAdapter(new MoviePagerAdapter(getSupportFragmentManager()));
		mTabLayout.setupWithViewPager(mViewPager);
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		switch (id) {
		// navigation drawer
		case android.R.id.home:
			mDrawerLayout.openDrawer(GravityCompat.START);
			return true;

		// Secondary menu
		case R.id.action_exit:
			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private NavigationView.OnNavigationItemSelectedListener onItemSelected =
		new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem) {
				switch (menuItem.getItemId()) {

					case R.id.navigation_item_movies:
						mViewPager.setAdapter(new MoviePagerAdapter(getSupportFragmentManager()));
						mTabLayout.setupWithViewPager(mViewPager);
						break;

					case R.id.navigation_item_tv_show:
						mViewPager.setAdapter(new TVPagerAdapter(getSupportFragmentManager()));
						mTabLayout.setupWithViewPager(mViewPager);
						break;

					case R.id.navigation_item_settings:
						Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
						startActivity(i);
						return true;
				}

				menuItem.setChecked(true);
				mDrawerLayout.closeDrawers();

				return true;
			}
		};

	private void setupNavigation() {
		setSupportActionBar(mToolbar);
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null) {
			actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		mNavigationView.setNavigationItemSelectedListener(onItemSelected);
	}

}
