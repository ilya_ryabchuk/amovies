package com.ilyarb.amovies.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ilyarb.amovies.Config;
import com.ilyarb.amovies.R;
import com.ilyarb.amovies.db.MovieHelper;
import com.ilyarb.amovies.loaders.MovieDetailLoader;
import com.ilyarb.amovies.models.Backdrop;
import com.ilyarb.amovies.models.Movie;
import com.ilyarb.amovies.models.Trailer;
import com.ilyarb.amovies.models.TrailerResultsPage;
import com.ilyarb.amovies.network.response.Response;
import com.ilyarb.amovies.ui.fragments.MovieTrailerFragment;
import com.ilyarb.amovies.ui.widgets.ShareItem;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class MovieDetailsActivity extends BaseDetailsActivityAbstract
	implements BaseDetailsActivityAbstract.UIHandler {

	private static final String TAG = "Movie Details Activity";

	@Bind(R.id.movie_title)
	TextView movieTitle;

	@Bind(R.id.movie_overview)
	TextView movieOverview;

	@Bind(R.id.homepage)
	TextView homepage;

	@Bind(R.id.movie_rate)
	RatingBar movieRate;

	@Bind(R.id.movie_poster)
	ImageView moviePoster;

	@Bind(R.id.release_date)
	TextView releaseDate;

	@Bind(R.id.budget)
	TextView budget;

	@Bind(R.id.horizontal_layout)
	LinearLayout horizontalLayout;

	@Bind(R.id.movie_tagline)
	TextView tagline;

	@Bind(R.id.movie_status)
	TextView movieStatus;

	@Bind(R.id.average_score)
	TextView averageScore;

	@Bind(R.id.vote_count)
	TextView voteCount;

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.movie_details);
		ButterKnife.bind(this);

		activityHandler = new ActivityHandler(this);

		setupActionBar();
		setDefaultValues();

		loadData(getIntent().getExtras(), R.id.movie_details_loader);
	}

	@Override public Loader<Response> onCreateLoader(int id, Bundle args) {
		return new MovieDetailLoader(getApplicationContext(), args.getInt(KEY));
	}

	@Override public void onLoadFinished(Loader<Response> loader, Response data) {
		final Movie movie = data.getTypedAnswer();

		if (movie != null) {
			setupUI(movie);
		} else {
			showError();
		}

		getLoaderManager().destroyLoader(loader.getId());
	}

	@Override public void onLoaderReset(Loader<Response> loader) {
		getLoaderManager().destroyLoader(loader.getId());
	}

	@Override public void handle(Message msg) {
		Movie movie = (Movie) msg.obj;
		showHeader();
		collapsingLayout.setTitle(movie.getTitle());
		setMovieTrailer(movie);
	}

	@Override protected void setDefaultValues() {
		budget.setText(R.string.default_field_value);
		releaseDate.setText(R.string.default_field_value);
		movieStatus.setText(R.string.default_field_value);
		homepage.setText(R.string.default_field_value);
	}

	@Override protected void loadFromCache() {
		int movieID = getIntent().getExtras().getInt(KEY);
		Movie cachedData = MovieHelper.get(Realm.getInstance(this), movieID);

		if (cachedData != null) {
			setupUI(cachedData);

		} else {
			showError();
		}
	}

	@OnClick(R.id.button_retry)
	protected void retry(View v) {
		reloadData(getIntent().getExtras(), R.id.movie_details_loader);
	}

	@OnClick(R.id.fab)
	protected void share(View v) {
		Intent i = new Intent(Intent.ACTION_SEND);
		String shareText = getString(R.string.share_text,
			shareItem.getText(),
			shareItem.getHomepage()
		);

		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_TEXT, shareText);

		startActivity(i);
	}

	private void setupUI(final Movie movie) {

		Picasso.with(this)
			.load(Config.IMAGE_URL + movie.getPosterPath())
			.fit()
			.centerCrop()
			.into(moviePoster);

		String score = getString(R.string.average_score, movie.getVoteAverage());
		String count = getString(R.string.vote_count, movie.getVoteCount());

		averageScore.setText(score);
		voteCount.setText(count);

		movieTitle.setText(movie.getTitle());
		movieOverview.setText(movie.getOverview());

		tagline.setText(movie.getTagline());
		movieStatus.setText(movie.getStatus());

		// Movie rate
		movieRate.setMax(10);
		movieRate.setNumStars(10);
		movieRate.setRating(movie.getVoteAverage());

		// Release date
		if (!movie.getReleaseDate().equals("")) {
			Log.d(TAG, movie.getReleaseDate());
			releaseDate.setText(movie.getReleaseDate());
		}

		// Homepage
		final String page = movie.getHomepage();
		if (!page.equals("")) {
			// home page exists
			homepage.setVisibility(View.VISIBLE);
			homepage.setText(page);
			// view page in browser
			homepage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(page));
					startActivity(i);
				}
			});
		}

		final List<Backdrop> images = movie.getImages().getBackdrops();
		if (images != null) {
			for (int i = 0; i < images.size(); i++) {
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT
				);

				final ImageView imageView = new ImageView(this);
				imageView.setId(i);
				imageView.setPadding(0, 2, 2, 0);
				imageView.setLayoutParams(params);

				Picasso.with(this)
					.load(Config.IMAGE_URL + images.get(i).getFilePath())
					.resize(500, 400)
					.into(imageView);

				horizontalLayout.addView(imageView);
			}
		}

		if (!movie.getBudget().equals("0")) {
			budget.setText(movie.getBudget());
		}

		// Set movie trailer in main thread
		Message msg = Message.obtain();
		msg.obj = movie;
		activityHandler.sendMessage(msg);

		// Set content to share
		shareItem = new ShareItem();

		shareItem.setText(movie.getTitle());
		shareItem.setHomepage(movie.getHomepage());
	}

	private void setMovieTrailer(final Movie movie) {
		final TrailerResultsPage response = movie.getTrailers();
		if (response != null) {
			final List<Trailer> trailers = response.getResults();

			if (trailers.size() != 0) {
				final String youtubeMovieID = trailers.get(0).getKey();
				MovieTrailerFragment f = MovieTrailerFragment.newInstance(youtubeMovieID);

				getSupportFragmentManager().beginTransaction()
					.add(R.id.movie_trailer_container, f)
					.commit();

			} else {
				findViewById(R.id.movie_trailer_container).setVisibility(View.GONE);
			}
		} else {
			findViewById(R.id.movie_trailer_container).setVisibility(View.GONE);
		}
	}

	private ShareItem shareItem;

}
