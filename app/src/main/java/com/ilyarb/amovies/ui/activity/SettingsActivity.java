package com.ilyarb.amovies.ui.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

import com.ilyarb.amovies.R;

import java.io.File;

public class SettingsActivity extends PreferenceActivity {

	private boolean notificationsDefault = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		Preference notifications = findPreference("pref_notify");
		Preference cache = findPreference("pref_clear_cache");

		cache.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				clearApplicationData();
				Toast.makeText(SettingsActivity.this, "Cache successful cleared", Toast.LENGTH_LONG)
					.show();

				return true;
			}
		});

		notifications.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {

				SharedPreferences prefs = getSharedPreferences("movies_shared_prefs", MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();

				editor.putBoolean("notifications", !notificationsDefault);
				return true;
			}
		});
	}

	private void clearApplicationData() {
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());

		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String dir : children) {
				if (!dir.equals("lib")) {
					deleteDir(new File(appDir, dir));
				}
			}
		}
	}

	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (String aChildren : children) {
				boolean success = deleteDir(new File(dir, aChildren));
				if (!success) {
					return false;
				}
			}
		}

		return dir.delete();
	}
}
