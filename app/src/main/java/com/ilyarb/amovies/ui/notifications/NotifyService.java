package com.ilyarb.amovies.ui.notifications;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ilyarb.amovies.R;

public class NotifyService extends Service {

	public static final int ID = 43545;
	private static final String TAG = "Notification Service";

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Log.d(TAG, "Starting service");

		MovieNotification n = new MovieNotification(getApplicationContext());
		n.setSmallIcon(R.mipmap.ic_launcher);
		n.setTitle("New movies incoming");
		n.setText("Checkout for latest movies");

		NotificationManager mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mManager.notify(ID, n.makeNotification());

		return START_STICKY;
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
