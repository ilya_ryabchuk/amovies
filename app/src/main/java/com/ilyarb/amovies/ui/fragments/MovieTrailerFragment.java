package com.ilyarb.amovies.ui.fragments;

import android.os.Bundle;
import android.util.Log;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.ilyarb.amovies.Config;

public class MovieTrailerFragment extends YouTubePlayerSupportFragment {

	private static final String TAG = "MovieTrailerFragment";

	public MovieTrailerFragment() { }

	public static MovieTrailerFragment newInstance(final String movieID) {
		MovieTrailerFragment f = new MovieTrailerFragment();

		Bundle args = new Bundle();
		args.putString("item_id", movieID);

		f.setArguments(args);
		f.init();

		return f;
	}

	private void init() {
		initialize(Config.DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {
			@Override
			public void onInitializationSuccess(
				YouTubePlayer.Provider provider,
				YouTubePlayer youTubePlayer, boolean b
			) {
				// start buffering
				if (!b) {
					youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
					youTubePlayer.cueVideo(getArguments().getString("item_id"));
				}
			}

			@Override
			public void onInitializationFailure(
				YouTubePlayer.Provider provider,
				YouTubeInitializationResult youTubeInitializationResult
			) {
				Log.e(TAG, "Failed to initalize youtube player : " + youTubeInitializationResult);
			}
		});
	}

}
