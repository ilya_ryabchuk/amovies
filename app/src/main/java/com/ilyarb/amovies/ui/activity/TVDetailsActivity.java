package com.ilyarb.amovies.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ilyarb.amovies.Config;
import com.ilyarb.amovies.R;
import com.ilyarb.amovies.loaders.TVDetailLoader;
import com.ilyarb.amovies.models.TVShow;
import com.ilyarb.amovies.network.response.Response;
import com.ilyarb.amovies.ui.widgets.ShareItem;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class TVDetailsActivity extends BaseDetailsActivityAbstract
	implements BaseDetailsActivityAbstract.UIHandler {

	@Bind(R.id.tv_title)
	TextView tvTitle;

	@Bind(R.id.tv_overview)
	TextView tvOverview;

	@Bind(R.id.tv_poster)
	ImageView tvPoster;

	@Bind(R.id.homepage)
	TextView homepage;

	@Bind(R.id.tv_rate)
	RatingBar tvRate;

	@Bind(R.id.release_date)
	TextView tvReleaseDate;

	@Bind(R.id.tv_episodes)
	TextView tvEpisodes;

	@Bind(R.id.tv_seasons)
	TextView tvSeasons;

	@Bind(R.id.tv_status)
	TextView tvStatus;

	private ShareItem shareItem;

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.tv_details);
		ButterKnife.bind(this);

		activityHandler = new ActivityHandler(this);

		setupActionBar();
		setDefaultValues();

		loadData(getIntent().getExtras(), R.id.tv_details_loader);
	}

	@Override public Loader<Response> onCreateLoader(int id, Bundle args) {
		return new TVDetailLoader(getApplicationContext(), args.getInt(KEY));
	}

	@Override public void onLoadFinished(Loader<Response> loader, Response data) {
		final TVShow show = data.getTypedAnswer();

		if (show != null) {
			setupUI(show);
		} else {
			showError();
		}

		getLoaderManager().destroyLoader(loader.getId());
	}

	@Override public void onLoaderReset(Loader<Response> loader) {
		getLoaderManager().destroyLoader(loader.getId());
	}

	@OnClick(R.id.button_retry)
	protected void retry(View v) {
		reloadData(getIntent().getExtras(), R.id.tv_details_loader);
	}

	@OnClick(R.id.fab)
	protected void share() {
		Intent i = new Intent(Intent.ACTION_SEND);
		String shareText = getString(R.string.share_text,
			shareItem.getText(),
			shareItem.getHomepage()
		);

		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_TEXT, shareText);

		startActivity(i);
	}

	@Override protected void setDefaultValues() {
		tvReleaseDate.setText(R.string.default_field_value);
		homepage.setText(R.string.default_field_value);
	}

	@Override protected void loadFromCache() {

		Realm realm = Realm.getInstance(this);
		int showID = getIntent().getExtras().getInt(KEY);

		TVShow cachedData = realm.where(TVShow.class)
			.equalTo("id", showID)
			.findFirst();

		if (cachedData != null) {
			setupUI(cachedData);
			//setDefaultValues();

		} else {
			showError();
		}
	}

	@Override public void handle(Message msg) {
		TVShow show = (TVShow) msg.obj;
		collapsingLayout.setTitle(show.getName());
		showHeader();
	}

	private void setupUI(TVShow show) {

		Picasso.with(this)
			.load(Config.IMAGE_URL + show.getPosterPath())
			.fit()
			.centerCrop()
			.into(tvPoster);

		tvTitle.setText(show.getName());
		tvOverview.setText(show.getOverview());
		tvStatus.setText(show.getStatus());

		String episodes = getString(R.string.episodes, show.getNumberOfEpisodes());
		String seasons = getString(R.string.seasons, show.getNumberOfSeasons());

		tvEpisodes.setText(episodes);
		tvSeasons.setText(seasons);

		tvRate.setMax(10);
		tvRate.setNumStars(10);
		tvRate.setRating(show.getVoteAverage());

		if (!show.getFirstAirDate().equals(""))
			tvReleaseDate.setText(show.getFirstAirDate());

		final String page = show.getHomepage();
		if (!page.equals("")) {
			// home page exists
			homepage.setVisibility(View.VISIBLE);
			homepage.setText(page);
			// view page in browser
			homepage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(page));
					startActivity(i);
				}
			});
		}

		Message msg = Message.obtain();
		msg.obj = show;
		activityHandler.sendMessage(msg);

		// Setting share content
		shareItem = new ShareItem();
		shareItem.setText(show.getName());
		shareItem.setHomepage(show.getHomepage());
	}

}
