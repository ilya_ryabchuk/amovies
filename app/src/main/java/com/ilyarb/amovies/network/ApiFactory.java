package com.ilyarb.amovies.network;

import android.support.annotation.NonNull;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.ilyarb.amovies.Config;

import java.util.Date;

import io.realm.RealmObject;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class ApiFactory {

	private static final Gson GSON = new GsonBuilder()
		.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
		.registerTypeAdapter(Date.class, new DateTypeAdapter())
		.setExclusionStrategies(new ExclusionStrategy() {
			@Override
			public boolean shouldSkipField(FieldAttributes f) {
				return f.getDeclaringClass().equals(RealmObject.class);
			}

			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}
		})
		.create();

	@NonNull
	public static IMovieService getMovieService() {
		return getRetrofit().create(IMovieService.class);
	}

	@NonNull
	public static ITVShowsService getTVService() {
		return getRetrofit().create(ITVShowsService.class);
	}

	@NonNull
	private static RestAdapter getRetrofit() {

		return new RestAdapter.Builder()
			.setEndpoint(Config.API_URL)
			.setLogLevel(RestAdapter.LogLevel.FULL)
			.setConverter(new GsonConverter(GSON))
			.setRequestInterceptor(new RequestInterceptor() {
				@Override public void intercept(RequestFacade request) {
					request.addHeader("Accept", "application/json");
				}
			})
			.build();
	}
}
