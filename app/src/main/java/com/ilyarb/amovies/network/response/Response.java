package com.ilyarb.amovies.network.response;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Response {

	@Nullable
	private Object mResponse;

	private RequestResult mRequestResult;

	public Response() {
		mRequestResult = RequestResult.ERROR;
	}

	@NonNull
	public RequestResult getRequestResult() {
		return mRequestResult;
	}

	public Response setRequestResult(RequestResult requestResult) {
		mRequestResult = requestResult;
		return this;
	}

	@Nullable
	public <T> T getTypedAnswer() {
		if (mResponse == null) {
			return null;
		}

		// noinspection unchecked
		return (T) mResponse;
	}

	public Response setAnswer(@Nullable Object response) {
		mResponse = response;
		return this;
	}

	public void save(Context context) {}

}
