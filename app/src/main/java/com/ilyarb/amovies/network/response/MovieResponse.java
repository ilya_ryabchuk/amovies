package com.ilyarb.amovies.network.response;

import android.content.Context;

import com.ilyarb.amovies.db.MovieHelper;
import com.ilyarb.amovies.models.MovieResultsPage;

import io.realm.Realm;

public class MovieResponse extends Response {

	@Override
	public void save(Context context) {
		MovieResultsPage page = getTypedAnswer();
		MovieHelper.savePage(Realm.getInstance(context), page);
	}

}
