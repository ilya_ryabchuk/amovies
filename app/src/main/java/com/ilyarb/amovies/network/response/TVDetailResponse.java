package com.ilyarb.amovies.network.response;

import android.content.Context;

import com.ilyarb.amovies.db.TVHelper;
import com.ilyarb.amovies.models.TVShow;

import io.realm.Realm;

public class TVDetailResponse extends Response {

	@Override
	public void save(Context context) {
		TVShow show = getTypedAnswer();
		TVHelper.save(Realm.getInstance(context), show);
	}
}
