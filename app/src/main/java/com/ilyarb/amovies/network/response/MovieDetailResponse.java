package com.ilyarb.amovies.network.response;

import android.content.Context;

import com.ilyarb.amovies.db.MovieHelper;
import com.ilyarb.amovies.models.Movie;

import io.realm.Realm;

public class MovieDetailResponse extends Response {

	@Override
	public void save(Context context) {
		Movie movie = getTypedAnswer();
		MovieHelper.save(Realm.getInstance(context), movie);
	}
}
