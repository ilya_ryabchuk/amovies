package com.ilyarb.amovies.network.response;

import android.content.Context;

import com.ilyarb.amovies.db.TVHelper;
import com.ilyarb.amovies.models.TVResultsPage;

import io.realm.Realm;

public class TVResponse extends Response {

	@Override
	public void save(Context context) {
		TVResultsPage page = getTypedAnswer();
		TVHelper.savePage(Realm.getInstance(context), page);
	}
}
