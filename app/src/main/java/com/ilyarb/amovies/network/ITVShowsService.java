package com.ilyarb.amovies.network;

import com.ilyarb.amovies.Config;
import com.ilyarb.amovies.models.TVResultsPage;
import com.ilyarb.amovies.models.TVShow;

import retrofit.http.GET;
import retrofit.http.Path;

public interface ITVShowsService {

	// get list of tv shows
	@GET("/tv/{type}?api_key=" + Config.API_KEY)
	TVResultsPage shows(@Path("type") String type);

	// get a show specified by id
	@GET("/tv/{id}?api_key=" + Config.API_KEY)
	TVShow show(@Path("id") int id);
}
