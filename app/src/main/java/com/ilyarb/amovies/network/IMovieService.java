package com.ilyarb.amovies.network;

import com.ilyarb.amovies.Config;
import com.ilyarb.amovies.models.Movie;
import com.ilyarb.amovies.models.MovieResultsPage;
import com.ilyarb.amovies.models.TrailerResultsPage;

import retrofit.http.GET;
import retrofit.http.Path;

public interface IMovieService {

	// Now playing movies
	@GET("/movie/{type}?api_key=" + Config.API_KEY)
	MovieResultsPage movies(@Path("type") String type);

	// Get a single movie by id
	@GET("/movie/{id}?append_to_response=images&api_key=" + Config.API_KEY)
	Movie movie(@Path("id") int id);

	// Get a movie trailers for movie by id
	@GET("/movie/{id}/videos?api_key=" + Config.API_KEY)
	TrailerResultsPage movieTrailers(@Path("id") int id);

}
