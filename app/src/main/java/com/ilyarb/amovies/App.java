package com.ilyarb.amovies;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;

import com.ilyarb.amovies.ui.notifications.NotifyService;

import java.util.Calendar;

public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
	}

	public void enableNotifications() {
		// Launching notification service
		Intent i = new Intent(getApplicationContext(), NotifyService.class);
		AlarmManager aManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, i, 0);
		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.HOUR_OF_DAY, 12);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);

		aManager.setRepeating(
			AlarmManager.RTC_WAKEUP,
			calendar.getTimeInMillis(),
			24 * 60 * 60 * 1000,
			pendingIntent
		);
	}
}
